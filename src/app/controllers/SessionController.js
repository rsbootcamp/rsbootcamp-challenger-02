import jwt from 'jsonwebtoken';
import User from '../models/User';
import * as Yup from 'yup';
import authConfig from '../../config/auth';

const loginValidation = async (req, res) => {
  const schema = Yup.object().shape({
    email: Yup.string()
      .email()
      .required(),
    password: Yup.string()
      .required()
      .min(6),
  });
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({
      error: 'Validation failed',
    });
  }
};

const emailAlreadyExist = async email => {
  return await User.findOne({ where: { email } });
};

const checkUserExist = async (req, res) => {
  if (!(await emailAlreadyExist(req.body.email))) {
    return res.status(401).json({
      error: 'User not found',
    });
  }
};
class SessionController {
  async store(req, res) {
    loginValidation(req, res);
    checkUserExist(req, res);
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email } });
    if (!(await user.checkPassword(password))) {
      return res.status(401).json({
        error: 'Password does not match',
      });
    }
    const { id, name } = user;
    return res.json({
      id,
      name,
      email,
      token: jwt.sign({ id }, authConfig.secret, {
        expiresIn: authConfig.expiresIn,
      }),
    });
  }
}

export default new SessionController();
