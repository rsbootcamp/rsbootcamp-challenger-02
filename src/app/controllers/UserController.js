import User from '../models/User';
import * as Yup from 'yup';

const createValidation = async (req, res) => {
  const schema = Yup.object().shape({
    name: Yup.string(),
    email: Yup.string()
      .email()
      .required(),
    password: Yup.string()
      .required()
      .min(6),
  });
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({
      error: 'Validation failed',
    });
  }
};
const updatePasswordValidation = async (req, res) => {
  const schema = Yup.object().shape({
    name: Yup.string(),
    email: Yup.string().email(),
    oldPassword: Yup.string()
      .required()
      .min(6),
    password: Yup.string()
      .required()
      .min(6),
    confirmPassword: Yup.string().when('password', (password, field) =>
      password ? field.required().oneOf([Yup.ref('password')]) : field
    ),
  });
  if (!(await schema.isValid(req.body))) {
    return res.status(400).json({
      error: 'Validation failed',
    });
  }
};
const emailAlreadyExist = async email => {
  return await User.findOne({ where: { email } });
};

const checkUserExist = async (req, res) => {
  if (await emailAlreadyExist(req.body.email)) {
    return res.status(401).json({
      error: 'User already exist',
    });
  }
};

class UserController {
  async store(req, res) {
    createValidation(req, res);
    checkUserExist(req, res);
    const user = await User.create(req.body);
    return res.json(user);
  }

  async updatePassword(req, res) {
    const { email, oldPassword, password } = req.body;
    updatePasswordValidation(req, res);
    const user = await User.findByPk(req.auth.id);
    if (!(await user.checkPassword(oldPassword))) {
      return res.status(401).json({
        error: 'Password does not match',
      });
    }
    return res.json(
      await user.update({
        password,
      })
    );
  }
}

export default new UserController();
